## Final Step 

![tux](images/tenor.gif)

#### Thanks for the preliminary submission! 

The last and final step will be an **Onboarding Meeting** hosted by our existing core team upon finishing your submission of all the tasks. It won't be a technical one. It is for getting to know you better, and to inform you more about FOSSNSS.

Consider the meeting as a friendly talk with us. Ask your queries and doubts during the meeting. We will mention what to expect while staying with FOSSNSS. Share your interests, we can work together to learn and grow. We cannot ensure a day when our team could meet offline.

## Core Team
A few highly passionate members who are ready to volunteer will be picked and taken into the Core Team. We will also invite you to our Matrix Group (Element), where all the discussions regarding FOSSNSS are going on. 

Try to use Linux as the primary operating system. It would be helpful if you want to engage in the amazing Free and Open Source Community.

#### After the completion, we welcome you to our team!

---
###### APPENDIX 

- Meet **Debbie, the Ant**. 
    **Debbie** is the official mascot of **FOSSNSS**.

<img src="images/debbie.png" height="300" />

- Apply for the **GitHub Student Developer**. More info can be found [here](https://education.github.com/pack).

- How to get started with **Open Source**. [Here is a nice resource](https://www.hackerearth.com/getstarted-opensource/).

---

### See you on the other side! 

